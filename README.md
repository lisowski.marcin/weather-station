# Weather Station

Home project for cheap weather station based on Atmega microcontrollers.

# Requirements

Visual Studio Code: https://code.visualstudio.com/download
Arduino CLI: https://arduino.github.io/arduino-cli/latest/installation/
VirtualWire library: http://electronoobs.com/eng_arduino_virtualWire.php

To install Arduino CLI on MacOS you can simply run:
```zsh
brew update
brew install arduino-cli
```
Make sure arduino-cli is added to `PATH` evironment variable. Then install required hardware packages. The required one is `arduino:avr`
```zsh
arduino-cli core install arduino:avr
```
Add `ARDUINO_AVR_SOURCE_DIR` environment variable and point it to Arduino AVR sources. This is required just for Visual Studio Code intelisense.
```zsh
export ARDUINO_AVR_SOURCE_DIR=/System/Volumes/Data/Users/marcin/Library/Arduino15/packages/arduino/hardware/avr/1.8.3
```
Add `ARDUINO_AVRGCC_BIN_DIR` environment variable and point it to Ardiuno tools directory. This is also required just for Visual Studio Code intelisense.
```zsh
export ARDUINO_AVRGCC_BIN_DIR=/Users/marcin/Library/Arduino15/packages/arduino/tools/avr-gcc/7.3.0-atmel3.6.1-arduino7/bin
```
Extract `VirtualWirte` to Arduino libraries directory (typically in `Documents/Arduino/libraries`). Setup `ARDUINO_LIBRARIES_DIR` environment variable with that path.
```zsh
export ARDUINO_LIBRARIES_DIR=~/Documents/Ardiuno/libraries
```