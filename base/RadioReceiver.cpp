#include "RadioReceiver.h"
#include <VirtualWire.h>

using namespace WeatherStation::Hardware;

namespace
{
    const int kTransferPin = 3;
}

void RadioReceiver::initialize(uint8_t bps)
{
    vw_set_rx_pin(kTransferPin);
    vw_setup(bps);
    vw_rx_start();
}

bool RadioReceiver::receive(ISerializable& message)
{
    const auto buffer = getTransmissionBuffer();
    auto length = getTransmissionBufferLength();

    if (vw_get_message(buffer, &length))
    {
#ifndef NDEBUG
        Serial.print("Received bytes: ");
        Serial.println(length);
#endif
        message.deserialize(buffer, length);
        return true;
    }
    return false;
}
