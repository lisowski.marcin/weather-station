#pragma once

#include <RadioModule.h>

namespace WeatherStation
{
    namespace Hardware
    {
        class RadioReceiver : public RadioModule
        {
        public:
            bool receive(ISerializable& message);

        protected:
            void initialize(uint8_t bps) override;
        };
    }
}
