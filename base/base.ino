#include "RadioReceiver.h"

#include <WeatherConditions.h>
#include <VirtualWire.h>

namespace
{
    WeatherStation::Hardware::RadioReceiver radio;
    unsigned long kLoopDelay = 1000;
}

void setup()
{
    radio.start();
#ifndef NDEBUG
    Serial.begin(9600);
#endif
}

void loop()
{
    WeatherStation::WeatherConditions conditions;

    if (radio.receive(conditions))
    {
#ifndef NDEBUG
        Serial.print("temperature: ");
        Serial.print(conditions.temperature);
        Serial.print(", humidity: ");
        Serial.print(conditions.humidity);
        Serial.print(", pressure: ");
        Serial.println(conditions.pressure);
#endif
    }

    delay(kLoopDelay);
}
