#include "WeatherConditions.h"
#include <assert.h>

using namespace WeatherStation;
using namespace WeatherStation::Hardware;

namespace
{
    const float kPrecisionFactor = 100.f;
}

void WeatherConditions::serialize(uint8_t* buffer, uint8_t length)
{
    assert(length == 6);
    int16_t* data = reinterpret_cast<int16_t*>(buffer);

    data[0] = temperature * kPrecisionFactor;
    data[1] = humidity * kPrecisionFactor;
    data[2] = pressure;
}

void WeatherConditions::deserialize(uint8_t* buffer, uint8_t length)
{
    assert(length == 6);
    int16_t* data = reinterpret_cast<int16_t*>(buffer);

    temperature = data[0] / kPrecisionFactor;
    humidity = data[1] / kPrecisionFactor;
    pressure = data[2];
}