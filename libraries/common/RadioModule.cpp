#include "RadioModule.h"

#include <VirtualWire.h>

using namespace WeatherStation::Hardware;

namespace
{
    const int kTransferBps = 2000;
    const int kTransferPin = 3;
    const int kBufferSize = 6;
    byte gTransmissionBuffer[kBufferSize] = {0};
}

void RadioModule::start()
{
    initialize(kTransferBps);
}

uint8_t* RadioModule::getTransmissionBuffer()
{
    return gTransmissionBuffer;
}

uint8_t RadioModule::getTransmissionBufferLength() const
{
    return kBufferSize;
}