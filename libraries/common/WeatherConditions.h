#include <RadioModule.h>

#pragma once

namespace WeatherStation
{
    struct WeatherConditions : Hardware::RadioModule::ISerializable
    {
        float temperature;
        float humidity;
        uint16_t pressure;

        void serialize(uint8_t* bufer, uint8_t length) override;
        void deserialize(uint8_t* bufer, uint8_t length) override;
    };
}
