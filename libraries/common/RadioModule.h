#include <stdint.h>

#pragma once

namespace WeatherStation
{
    namespace Hardware
    {
        class RadioModule
        {
        public:
            struct ISerializable
            {
                virtual void serialize(uint8_t* bufer, uint8_t length) = 0;
                virtual void deserialize(uint8_t* bufer, uint8_t length) = 0;
            };

            void start();

        protected:
            virtual void initialize(uint8_t bps) = 0;

            uint8_t* getTransmissionBuffer();
            uint8_t getTransmissionBufferLength() const;
        };
    }
}
