#include "RadioTransmitter.h"
#include <WeatherConditions.h>

namespace
{
    WeatherStation::WeatherConditions conditions;
    WeatherStation::Hardware::RadioTransmitter radio;
    unsigned long kLoopDelay = 1000;
}

void setup()
{
    pinMode(LED_BUILTIN, OUTPUT);
    radio.start();

#ifndef NDEBUG
    conditions.temperature = -40.4f;
    conditions.humidity = 42.71f;
    conditions.pressure = 0;

    Serial.begin(9600);
#endif
}

void loop()
{
    digitalWrite(LED_BUILTIN, HIGH);
    radio.send(conditions);

#ifndef NDEBUG
    conditions.temperature += 4.13f;
    conditions.humidity += 0.7f;
    conditions.pressure += 25;

    Serial.print("temperature: ");
    Serial.print(conditions.temperature);
    Serial.print(", humidity: ");
    Serial.print(conditions.humidity);
    Serial.print(", pressure: ");
    Serial.println(conditions.pressure);
#endif

    delay(kLoopDelay);
}
