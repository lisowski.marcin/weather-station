#pragma once

#include <stdint.h>
#include <RadioModule.h>

namespace WeatherStation
{
    namespace Hardware
    {
        class RadioTransmitter : public RadioModule
        {
        public:
            bool send(ISerializable& message);

        protected:
            void initialize(uint8_t bps) override;
        };
    }
}
