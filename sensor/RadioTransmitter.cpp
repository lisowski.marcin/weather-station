#include "RadioTransmitter.h"
#include <VirtualWire.h>

using namespace WeatherStation::Hardware;

namespace
{
    const int kTransferPin = 3;
}

void RadioTransmitter::initialize(uint8_t bps)
{
    vw_set_tx_pin(kTransferPin);
    vw_setup(bps);
}

bool RadioTransmitter::send(ISerializable& message)
{
    const auto buffer = getTransmissionBuffer();
    const auto length = getTransmissionBufferLength();

    message.serialize(buffer, length);
    const uint8_t result = vw_send(buffer, length);

    if (!result)
    {
        return false;
    }

    vw_wait_tx();
    return true;
}